package nom.brunokarpo.crudStart.repository.jpa;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import nom.brunokarpo.crudStart.modelo.Cliente;
import nom.brunokarpo.crudStart.modelo.ClienteBuilder;
import nom.brunokarpo.crudStart.modelo.Sexo;
import nom.brunokarpo.crudStart.repository.exception.ObjetoNaoEncontradoException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class ClientesJpaImplTest {

	@Mock
	private ClientesJpaImpl sut; // Stub under test

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void deveInserirUmObjetoClienteAtravesDoRepository() throws Exception {
		Cliente cliente = criarClienteParaTeste();

		sut.cadastrar(cliente);
		verify(sut).cadastrar(cliente);
	}

	@Test
	public void deveConsultarUmObjetoClienteAtravesDoRepository() throws Exception {

		Cliente esperado = criarClienteParaTeste();
		esperado.setCodigo(1L);

		when(sut.buscarPorCodigo(1L)).thenReturn(esperado);

		Cliente retornado = sut.buscarPorCodigo(1L);

		assertEquals(1L, retornado.getCodigo().longValue());
		assertEquals("Bruno Nogueira", retornado.getNome());
		assertEquals("Desenvolvedor de Sistemas", retornado.getProfissao());
		assertEquals(25, retornado.getIdade(), 0.00001);
		assertEquals(Sexo.MASCULINO, retornado.getSexo());
	}

	@Test(expected=ObjetoNaoEncontradoException.class)
	public void deveRetornarExceptionNaoEncontradoQuandoBuscarClienteNaoExistir() throws Exception {
		when(sut.buscarPorCodigo(30L)).thenThrow(new ObjetoNaoEncontradoException());

		sut.buscarPorCodigo(30L);

	}

	@Test
	public void deveExcluirUmClienteDoBanco() throws Exception {
		Cliente resposta = criarClienteParaTeste();
		resposta.setCodigo(1L);

		when(sut.excluirPorCodigo(1L)).thenReturn(resposta);

		Cliente retorno = sut.excluirPorCodigo(1L);

		verify(sut).excluirPorCodigo(1L);
		assertEquals(resposta, retorno);

	}

	@Test(expected=ObjetoNaoEncontradoException.class)
	public void deveLancarExceptionQuandoNaoEncontrarObjetoNoBanco() throws Exception {
		when(sut.excluirPorCodigo(30L)).thenThrow(new ObjetoNaoEncontradoException());

		sut.excluirPorCodigo(30L);

	}

	@Test
	public void deveAtualizarClientePeloCodigo() throws Exception {
		Cliente resposta = criarClienteParaTeste();
		resposta.setCodigo(3L);

		when(sut.buscarPorCodigo(3L)).thenReturn(resposta);

		Cliente cliente = sut.buscarPorCodigo(3L);

		cliente.setNome("Ariana Lokaut");
		cliente.setSexo(Sexo.FEMININO);
		cliente.setIdade(22);
		cliente.setProfissao("Stripper");

		sut.atualizar(cliente);

		verify(sut).atualizar(cliente);

	}

	private Cliente criarClienteParaTeste() {
		return new ClienteBuilder().comNome("Bruno Nogueira")
									.comIdade(25)
									.comProfissao("Desenvolvedor de Sistemas")
									.comSexo(Sexo.MASCULINO)
									.construir();
	}

}
