package nom.brunokarpo.crudStart.repository.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import nom.brunokarpo.crudStart.modelo.Cliente;
import nom.brunokarpo.crudStart.repository.Clientes;
import nom.brunokarpo.crudStart.repository.exception.ObjetoNaoEncontradoException;

public class ClientesJpaImpl implements Clientes {

	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("exemplo-pu");
	private EntityManager em = emf.createEntityManager();

	@Override
	public void cadastrar(Cliente cliente) {

		em.getTransaction().begin();
		em.persist(cliente);
		em.getTransaction().commit();

		em.close();
	}

	public Cliente buscarPorCodigo(long codigo) throws ObjetoNaoEncontradoException {

		Cliente cliente = em.find(Cliente.class, codigo);

		if(cliente == null) {
			throw new ObjetoNaoEncontradoException("Nao encontrei esse cliente no banco");
		}

		return cliente;
	}

	public Cliente excluirPorCodigo(long codigo) throws ObjetoNaoEncontradoException {

		Cliente cliente = buscarPorCodigo(codigo);

		em.getTransaction().begin();
		em.remove(cliente);
		em.getTransaction().commit();

		return cliente;
	}

	public void atualizar(Cliente cliente) {
		em.getTransaction().begin();
		em.merge(cliente);
		em.getTransaction().commit();
	}

}
