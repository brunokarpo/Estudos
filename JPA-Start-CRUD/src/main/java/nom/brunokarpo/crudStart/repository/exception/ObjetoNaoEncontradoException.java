package nom.brunokarpo.crudStart.repository.exception;

public class ObjetoNaoEncontradoException extends Exception{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public ObjetoNaoEncontradoException() {
		super();
	}

	public ObjetoNaoEncontradoException(String message) {
		super(message);
	}
}
