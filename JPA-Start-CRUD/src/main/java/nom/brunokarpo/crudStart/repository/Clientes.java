package nom.brunokarpo.crudStart.repository;

import nom.brunokarpo.crudStart.modelo.Cliente;
import nom.brunokarpo.crudStart.repository.exception.ObjetoNaoEncontradoException;

public interface Clientes {

	public void cadastrar(Cliente cliente);

	public Cliente buscarPorCodigo(long codigo) throws ObjetoNaoEncontradoException;

	public Cliente excluirPorCodigo(long codigo) throws ObjetoNaoEncontradoException;

	public void atualizar(Cliente cliente);

}
