package nom.brunokarpo.crudStart.modelo;

public class ClienteBuilder {

	private Cliente instancia;

	public ClienteBuilder() {
		instancia = new Cliente();
	}

	public ClienteBuilder comNome(String nome) {
		instancia.setNome(nome);
		return this;
	}

	public ClienteBuilder comIdade(Integer idade) {
		instancia.setIdade(idade);
		return this;
	}

	public ClienteBuilder comSexo(Sexo sexo) {
		instancia.setSexo(sexo);
		return this;
	}

	public ClienteBuilder comProfissao(String profissao) {
		instancia.setProfissao(profissao);
		return this;
	}

	public ClienteBuilder comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		return this;
	}

	public Cliente construir() {
		return instancia;
	}

}
