package nom.brunokarpo.agendaTelefonica.modelo;

import java.util.Date;

public class AgendaBuilder {

	private Agenda instance;

	public AgendaBuilder() {
		this.instance = new Agenda();
	}

	public AgendaBuilder comNome(String nome) {
		this.instance.setNome(nome);
		return this;
	}

	public AgendaBuilder comTelefone(String telefone) {
		this.instance.setTelefone(telefone);
		return this;
	}

	public AgendaBuilder comDataRegistro(Date dataRegistro) {
		this.instance.setDataRegistro(dataRegistro);
		return this;
	}

	public Agenda construir() {
		return this.instance;
	}
}
