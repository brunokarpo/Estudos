package nom.brunokarpo.agendaTelefonica.services.exceptions;

public class ContatoInexistenteException extends Exception {

	private static final long serialVersionUID = 1L;

	public ContatoInexistenteException() {
		super();
	}
 }
