package nom.brunokarpo.agendaTelefonica.services;

import nom.brunokarpo.agendaTelefonica.modelo.Agenda;
import nom.brunokarpo.agendaTelefonica.repositorio.IAgendaRepository;
import nom.brunokarpo.agendaTelefonica.services.exceptions.ContatoInexistenteException;

public class AgendaService {

	private IAgendaRepository repositorio;

	public AgendaService(IAgendaRepository implementacao) {
		repositorio = implementacao;
	}

	public void adicionarContato(Agenda contato) {
		repositorio.inserirContato(contato);
	}

	public Agenda buscarPorCodigo(Long codigo) throws ContatoInexistenteException {
		Agenda contato = repositorio.buscarPorCodigo(codigo);

		if(contato == null) {
			throw new ContatoInexistenteException();
		}

		return contato;
	}

	public void excluirPorCodigo(Long codigo) {
		Agenda contato = repositorio.buscarPorCodigo(codigo);

		if (contato != null) {
			repositorio.excluir(contato);
		}
	}

	public void atualizarContato(Agenda contato) throws ContatoInexistenteException {
		Agenda existente = buscarPorCodigo(contato.getCodigo());

		existente = contato;

		adicionarContato(contato);
	}

}
