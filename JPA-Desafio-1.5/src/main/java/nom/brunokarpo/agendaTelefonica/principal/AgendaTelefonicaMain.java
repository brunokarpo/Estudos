package nom.brunokarpo.agendaTelefonica.principal;

import java.util.Calendar;

import nom.brunokarpo.agendaTelefonica.modelo.Agenda;
import nom.brunokarpo.agendaTelefonica.modelo.AgendaBuilder;
import nom.brunokarpo.agendaTelefonica.repositorio.IAgendaRepository;
import nom.brunokarpo.agendaTelefonica.repositorio.jpa.AgendaRepositoryJPA;
import nom.brunokarpo.agendaTelefonica.services.AgendaService;
import nom.brunokarpo.agendaTelefonica.services.exceptions.ContatoInexistenteException;

public class AgendaTelefonicaMain {

	private AgendaService services;

	AgendaTelefonicaMain() {
		execute();
	}


	private void execute() {
		IAgendaRepository repositorio = new AgendaRepositoryJPA();
		services = new AgendaService(repositorio);

		Agenda contato = null;

		// Garantindo inserção
		contato = criarContato();
		services.adicionarContato(contato);

		Long codigo = contato.getCodigo();

		contato = null;
		try {
			contato = services.buscarPorCodigo(codigo);
			System.out.println(contato);

			contato.setNome("Arn Magnusson");

			services.atualizarContato(contato);

		} catch (ContatoInexistenteException e) {
			System.out.println("Contato nao existe no Banco de Dados");
		}

		services.excluirPorCodigo(codigo);
	}


	private Agenda criarContato() {
		return new AgendaBuilder().comNome("Bruno Nogueira")
								.comTelefone("9999-8888")
								.comDataRegistro(Calendar.getInstance().getTime())
								.construir();
	}


	public static void main(String[] args) {
		new AgendaTelefonicaMain();
	}

}
