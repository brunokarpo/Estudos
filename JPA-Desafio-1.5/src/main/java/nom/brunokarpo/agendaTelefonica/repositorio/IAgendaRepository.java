package nom.brunokarpo.agendaTelefonica.repositorio;

import nom.brunokarpo.agendaTelefonica.modelo.Agenda;

public interface IAgendaRepository {

	void inserirContato(Agenda contato);

	Agenda buscarPorCodigo(Long codigo);

	void excluir(Agenda contato);

}
