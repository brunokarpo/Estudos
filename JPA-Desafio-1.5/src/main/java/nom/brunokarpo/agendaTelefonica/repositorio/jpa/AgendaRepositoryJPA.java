package nom.brunokarpo.agendaTelefonica.repositorio.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import nom.brunokarpo.agendaTelefonica.modelo.Agenda;
import nom.brunokarpo.agendaTelefonica.repositorio.IAgendaRepository;

public class AgendaRepositoryJPA implements IAgendaRepository {

	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("agendaTelefonicaPu");
	private EntityManager em;

	@Override
	public void inserirContato(Agenda contato) {
		em = emf.createEntityManager();

		em.getTransaction().begin();
		em.merge(contato);
		em.getTransaction().commit();

		em.close();
	}

	@Override
	public Agenda buscarPorCodigo(Long codigo) {
		em = emf.createEntityManager();

		Agenda contato = em.find(Agenda.class, codigo);

		return contato;
	}

	@Override
	public void excluir(Agenda contato) {
		em.getTransaction().begin();
		em.remove(contato);
		em.getTransaction().commit();

		em.close();
	}

}
