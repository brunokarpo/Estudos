package nom.brunokarpo.agendaTelefonica.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Calendar;

import nom.brunokarpo.agendaTelefonica.modelo.Agenda;
import nom.brunokarpo.agendaTelefonica.modelo.AgendaBuilder;
import nom.brunokarpo.agendaTelefonica.repositorio.IAgendaRepository;
import nom.brunokarpo.agendaTelefonica.services.exceptions.ContatoInexistenteException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class AgendaServiceTest {

	private AgendaService sut;

	@Mock
	private IAgendaRepository mock;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		sut = new AgendaService(mock);
	}

	@Test
	public void deveInserirUmContatoNaAgenda() throws Exception {
		Agenda contato = criarContatoParaTeste();

		sut.adicionarContato(contato);

		verify(mock).inserirContato(contato);
	}

	@Test
	public void deveBuscarUmContatoPeloCodigo() throws Exception {
		Agenda resposta = criarContatoParaTeste();
		when(mock.buscarPorCodigo(1L)).thenReturn(resposta);

		Agenda retorno = sut.buscarPorCodigo(1L);

		assertEquals(resposta, retorno);
	}

	@Test(expected=ContatoInexistenteException.class)
	public void deveRetornarExceptionDeContatoInexistenteQuandoBuscarContatoPorCodigoInexistente() throws Exception {
		when(mock.buscarPorCodigo(1L)).thenReturn(null);

		sut.buscarPorCodigo(1L);
	}

	@Test
	public void deveExcluirContatoPorCodigo() throws Exception {
		Agenda resposta = criarContatoParaTeste();
		when(mock.buscarPorCodigo(1L)).thenReturn(resposta);

		sut.excluirPorCodigo(1L);
	}

	@Test
	public void deveAtualizarONomeDoContato() throws Exception {
		Agenda resposta = criarContatoParaTeste();
		when(mock.buscarPorCodigo(1L)).thenReturn(resposta);

		Agenda contato = new Agenda();
		contato.setCodigo(1L);
		contato.setNome("Arn Magnusson");

		sut.atualizarContato(contato);
	}

	private Agenda criarContatoParaTeste() {
		return new AgendaBuilder().comNome("Bruno Nogueira")
											.comTelefone("9999-8888")
											.comDataRegistro(Calendar.getInstance().getTime())
											.construir();
	}

}
