Repositórios de Estudos
=======

Repositório de códigos fontes da aplicação de conhecimentos adquiridos através de estudos, workshops, vídeos-aulas, livros, etc.
Esse repositório está sob construção contínua em uma máquina virtual local que, dentro em breve estará disponível em ambiente aberto da Web;

Leia nossa Wiki para conhecer nosso processo;

> By: Bruno Nogueira de Oliveira Graduando em Sistemas de Informação
> pela Universidade Federal de Goiás
