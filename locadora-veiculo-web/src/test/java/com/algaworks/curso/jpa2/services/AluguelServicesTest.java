package com.algaworks.curso.jpa2.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.algaworks.curso.jpa2.dao.AluguelDao;
import com.algaworks.curso.jpa2.modelo.Aluguel;
import com.algaworks.curso.jpa2.modelo.ApoliceSeguro;
import com.algaworks.curso.jpa2.modelo.Carro;
import com.algaworks.curso.jpa2.modelo.builders.AluguelBuilder;
import com.algaworks.curso.jpa2.modelo.builders.ApoliceSeguroBuilder;
import com.algaworks.curso.jpa2.modelo.builders.CarroBuilder;
import com.algaworks.curso.jpa2.services.exception.RegraNegocioException;

public class AluguelServicesTest {

	private static final String MENSAGEM_VALOR_ALUGUEL_INVALIDO = "Valor total do Aluguel inválido. Informe um valor positivo diferente de zero";
	private static final String MENSAGEM_ALUGUEL_SEM_CARRO = "Um aluguel não pode ser persistido se não tiver um carro. Informe um carro";
	private static final String MENSAGEM_ALUGUEL_CARRO_INEXISTENTE = "Carro não existe no banco. Informe um carro válido;";
	private static final String MENSAGEM_APOLICE_COM_SEGURO_E_ZERADA = "Apolice contém seguros e não pode estar com a franquia zerada";
	private static final String MENSAGEM_APOLICE_SEM_SEGURO_E_COBRADA = "Apolice sem seguros. Não deve ser cobrado franquia";

	private AluguelService sut;

	@Mock private AluguelDao daoMock;
	@Mock private CarroServices carroServiceMock;

	private Aluguel aluguel;
	private Carro carro;
	private ApoliceSeguro apoliceSeguro;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		sut = new AluguelService();
		sut.setAluguelDao(daoMock);
		sut.setCarroService(carroServiceMock);

		construirObjetosComBuilder();

		when(carroServiceMock.buscarPorCodigo(1L)).thenReturn(carro);
	}

	@Test
	public void deveSalvarUmAluguelValidoNoBanco() throws Exception {
		sut.salvar(aluguel);

		verify(daoMock).salvar(aluguel);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersitirUmAluguelCujoValorTotalEstejaZerado() throws Exception {
		aluguel.setValorTotal(BigDecimal.ZERO);

		validarExceptionComMensagemDeValidacao(MENSAGEM_VALOR_ALUGUEL_INVALIDO);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmAluguelCujoValorTotalSejaNegativo() throws Exception {
		aluguel.setValorTotal(new BigDecimal(-1));

		validarExceptionComMensagemDeValidacao(MENSAGEM_VALOR_ALUGUEL_INVALIDO);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmAluguelCujoValorTotalSejaNulo() throws Exception {
		aluguel.setValorTotal(null);

		validarExceptionComMensagemDeValidacao(MENSAGEM_VALOR_ALUGUEL_INVALIDO);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmAluguelSemCarroInformado() throws Exception {
		aluguel.setCarro(null);

		validarExceptionComMensagemDeValidacao(MENSAGEM_ALUGUEL_SEM_CARRO);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmAluguelSeCarroInformadoNaoExistirNoBanco() throws Exception {
		when(carroServiceMock.buscarPorCodigo(1L)).thenReturn(null);

		validarExceptionComMensagemDeValidacao(MENSAGEM_ALUGUEL_CARRO_INEXISTENTE);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDeveSalvarAluguelSeApoliceDeSeguroEstiverComValorZeradoEAlgumSeguroHabilitado() throws Exception {
		aluguel.getApoliceSeguro().setValorFranquia(BigDecimal.ZERO);

		validarExceptionComMensagemDeValidacao(MENSAGEM_APOLICE_COM_SEGURO_E_ZERADA);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDeveSalvarAluguelSeApoliceSemSeguroEComFranquiaCobrada() throws Exception {
		ApoliceSeguro situacao = new ApoliceSeguroBuilder().comProtecaoCausasNaturais(false)
															.comProtecaoRoubo(false)
															.comProtecaoTerceiros(false)
															.comValorFranquia(new BigDecimal(100))
															.construir();

		aluguel.setApoliceSeguro(situacao);

		validarExceptionComMensagemDeValidacao(MENSAGEM_APOLICE_SEM_SEGURO_E_COBRADA);
	}

	private void validarExceptionComMensagemDeValidacao(String mensagemException) throws RegraNegocioException {
		try {
			sut.salvar(aluguel);
		} catch(RegraNegocioException e) {
			assertEquals(mensagemException, e.getMessage());
			throw new RegraNegocioException(mensagemException);
		}
	}


	private void construirObjetosComBuilder() {
		carro = new CarroBuilder().comCodigo(1L)
				.construir();

		apoliceSeguro = new ApoliceSeguroBuilder().comCodigo(1L)
				.comProtecaoCausasNaturais(true)
				.comProtecaoRoubo(false)
				.comProtecaoTerceiros(true)
				.comValorFranquia(new BigDecimal(850))
				.construir();

		aluguel = new AluguelBuilder().comCodigo(1L)
				.comValorTotal(new BigDecimal(250.0))
				.comCarro(carro)
				.comApoliceSeguro(apoliceSeguro)
				.construir();
	}

}
