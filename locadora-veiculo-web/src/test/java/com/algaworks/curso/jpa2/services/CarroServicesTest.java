package com.algaworks.curso.jpa2.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.algaworks.curso.jpa2.dao.CarroDao;
import com.algaworks.curso.jpa2.modelo.Acessorio;
import com.algaworks.curso.jpa2.modelo.Carro;
import com.algaworks.curso.jpa2.modelo.ModeloCarro;
import com.algaworks.curso.jpa2.modelo.builders.AcessorioBuilder;
import com.algaworks.curso.jpa2.modelo.builders.CarroBuilder;
import com.algaworks.curso.jpa2.modelo.builders.ModeloCarroBuilder;
import com.algaworks.curso.jpa2.services.exception.RegraNegocioException;

public class CarroServicesTest {

	private static final String MENSAGEM_ERRO_CARRO_SEM_PLACA = "O carro precisa ter uma placa";
	private static final String MENSAGEM_ERRO_CARRO_SEM_CHASSI = "O carro precisa ter um chassi";
	private static final String MENSAGEM_ERRO_CARRO_SEM_COR = "Informe a cor do carro";
	private static final String MENSAGEM_ERRO_CARRO_DIARIA_INVALIDA = "Valor da diária inválido. Informe um valor positivo diferente de zero";
	private static final String MENSAGEM_ERRO_CARRO_SEM_MODELO = "O carro precisa ter um modelo";
	private static final String MENSAGEM_ERRO_MODELO_INEXISTENTE = "Modelo informado não existe";
	private static final String MENSAGEM_ERRO_ACESSORIOS_INEXISTENTES = "O(s) acessório(s) Banco de couro, Vidros Elétricos, Travas elétricas, Direção Hidráulica não existe(m)";
	private static final String MENSAGEM_ERRO_CARRO_INEXISTENTE = "O carro não existe no Banco de Dados e não pode ser excluído";

	private CarroServices sut;

	@Mock private CarroDao daoMock;
	@Mock private ModeloCarroService modeloServiceMock;
	@Mock private AcessorioServices acessorioServiceMock;

	private Carro carro;


	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		carro = new CarroBuilder().comCodigo(1L)
								.comChassi("abc123def456ghi789")
								.comPlaca("AAA-1234")
								.comCor("Branca")
								.comValorDiaria(new BigDecimal(250))
								.comModelo(criarModeloCarro())
								.comAcessorios(gerarAcessorios())
								.construir();

		sut = new CarroServices();
		sut.setCarroDao(daoMock);
		sut.setModeloCarroServices(modeloServiceMock);
		sut.setAcessorioServices(acessorioServiceMock);

		when(modeloServiceMock.buscarTodos()).thenReturn(gerarModelos());
		when(acessorioServiceMock.buscarTodos()).thenReturn(gerarAcessorios());
	}

	@Test
	public void devePersitirUmCarro() throws Exception {
		when(modeloServiceMock.buscarTodos()).thenReturn(gerarModelos());

		sut.salvar(carro);

		verify(daoMock).salvar(carro);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmCarroQueTenhaPlacaNula() throws Exception {
		carro.setPlaca(null);

		validarSalvacaoComException(MENSAGEM_ERRO_CARRO_SEM_PLACA);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmCarroQueTenhaPlacaVazia() throws Exception {
		carro.setPlaca("  	  ");

		validarSalvacaoComException(MENSAGEM_ERRO_CARRO_SEM_PLACA);
	}


	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmCarroSemChassi() throws Exception {
		carro.setChassi(null);

		validarSalvacaoComException(MENSAGEM_ERRO_CARRO_SEM_CHASSI);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmCarroComChassiVazio() throws Exception {
		carro.setChassi("  	 ");

		validarSalvacaoComException(MENSAGEM_ERRO_CARRO_SEM_CHASSI);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmCarroSemCor() throws Exception {
		carro.setCor(null);

		validarSalvacaoComException(MENSAGEM_ERRO_CARRO_SEM_COR);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmCarroComCorVazia() throws Exception {
		carro.setCor("  	 ");

		validarSalvacaoComException(MENSAGEM_ERRO_CARRO_SEM_COR);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersitirUmCarroCujoValorDaDiariaEstiverNulo() throws Exception {
		carro.setValorDiaria(null);

		validarSalvacaoComException(MENSAGEM_ERRO_CARRO_DIARIA_INVALIDA);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersitirUmCarroCujoValorDaDiariaSejaNegativo() throws Exception {
		carro.setValorDiaria(new BigDecimal(-150.0));

		validarSalvacaoComException(MENSAGEM_ERRO_CARRO_DIARIA_INVALIDA);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersitirUmCarroCujoValorDaDiariaEstejaZerado() throws Exception {
		carro.setValorDiaria(BigDecimal.ZERO);

		validarSalvacaoComException(MENSAGEM_ERRO_CARRO_DIARIA_INVALIDA);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmCarroSemModelo() throws Exception {
		carro.setModeloCarro(null);

		validarSalvacaoComException(MENSAGEM_ERRO_CARRO_SEM_MODELO);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmCarroCujoModeloNaoExistaNoBanco() throws Exception {
		when(modeloServiceMock.buscarTodos()).thenReturn(new ArrayList<ModeloCarro>());

		validarSalvacaoComException(MENSAGEM_ERRO_MODELO_INEXISTENTE);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmCarroCujoAlgumDosAcessoriosnaoExistirNoBanco() throws Exception {
		List<Acessorio> acessoriosBanco = new ArrayList<Acessorio>();
		acessoriosBanco.add(new AcessorioBuilder().comCodigo(2L).comDescricao("Ar condicionado").construir());

		when(acessorioServiceMock.buscarTodos()).thenReturn(acessoriosBanco);

		validarSalvacaoComException(MENSAGEM_ERRO_ACESSORIOS_INEXISTENTES);
	}

	@Test
	public void deveBuscarTodosOsCarrosDoBanco() throws Exception {
		when(daoMock.buscarTodos()).thenReturn(criarListaCarro());

		List<Carro> carrosBanco = sut.buscarTodos();

		assertEquals(4, carrosBanco.size());
	}

	@Test
	public void deveBuscarUmCarroPeloCodigo() throws Exception {
		when(daoMock.buscarPorCodigo(carro.getCodigo())).thenReturn(new CarroBuilder().comCodigo(1L).construir());

		Carro retorno = sut.buscarPorCodigo(carro.getCodigo());

		verify(daoMock).buscarPorCodigo(carro.getCodigo());
		assertEquals(carro, retorno);
	}

	@Test
	public void deveExcluirUmCarroPassadoPorParametro() throws Exception {
		when(daoMock.buscarPorCodigo(1L)).thenReturn(carro);

		sut.excluir(carro);

		verify(daoMock).excluir(carro);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDeveExcluirUmCarroQueNaoExisteNoBanco() throws Exception {
		when(daoMock.buscarPorCodigo(1L)).thenReturn(null);

		validarExclusaoComException(MENSAGEM_ERRO_CARRO_INEXISTENTE);
	}

	@Test
	public void deveRetornarUmCarroComSeusDevidosAcessorios() throws Exception {
		when(daoMock.buscarCarroComAcessorios(carro.getCodigo())).thenReturn(this.carro);

		Carro carroTemp = daoMock.buscarCarroComAcessorios(this.carro.getCodigo());

		assertArrayEquals("Validando lista de acessorios", gerarAcessorios().toArray(), carroTemp.getAcessorios().toArray());
	}

	private List<Carro> criarListaCarro() {
		List<Carro> carros = new ArrayList<Carro>();

		carros.add(new CarroBuilder().comCodigo(1L).construir());
		carros.add(new CarroBuilder().comCodigo(2L).construir());
		carros.add(new CarroBuilder().comCodigo(3L).construir());
		carros.add(new CarroBuilder().comCodigo(4L).construir());

		return carros;
	}

	private ModeloCarro criarModeloCarro() {
		return new ModeloCarroBuilder().comCodigo(10L).comDescricao("Sentra").comFabricante("Chevrolet").construir();
	}

	private List<Acessorio> gerarAcessorios() {
		List<Acessorio> acessorios = new ArrayList<Acessorio>();
		acessorios.add(new AcessorioBuilder().comCodigo(1L).comDescricao("Banco de couro").construir());
		acessorios.add(new AcessorioBuilder().comCodigo(2L).comDescricao("Ar condicionado").construir());
		acessorios.add(new AcessorioBuilder().comCodigo(3L).comDescricao("Vidros Elétricos").construir());
		acessorios.add(new AcessorioBuilder().comCodigo(4L).comDescricao("Travas elétricas").construir());
		acessorios.add(new AcessorioBuilder().comCodigo(5L).comDescricao("Direção Hidráulica").construir());
		return acessorios;
	}

	private List<ModeloCarro> gerarModelos() {
		List<ModeloCarro> modelos = new ArrayList<ModeloCarro>();
		modelos.add(new ModeloCarroBuilder().comCodigo(1L).comDescricao("Classe B").comFabricante("Mercedes Bens").construir());
		modelos.add(new ModeloCarroBuilder().comCodigo(2L).comDescricao("Vectra").comFabricante("Chevrolet").construir());
		modelos.add(new ModeloCarroBuilder().comCodigo(3L).comDescricao("Civic").comFabricante("Honda").construir());
		modelos.add(new ModeloCarroBuilder().comCodigo(4L).comDescricao("HB 20").comFabricante("Hyundai").construir());
		modelos.add(new ModeloCarroBuilder().comCodigo(5L).comDescricao("Gol").comFabricante("Volkswagen").construir());
		modelos.add(new ModeloCarroBuilder().comCodigo(6L).comDescricao("Verona").comFabricante("Ford").construir());
		modelos.add(new ModeloCarroBuilder().comCodigo(10L).comDescricao("Sentra").comFabricante("Chevrolet").construir());
		return modelos;
	}

	private void validarSalvacaoComException(String mensagemErroEsperada) throws RegraNegocioException {
		try {
			sut.salvar(carro);

		} catch(RegraNegocioException e) {
			assertEquals(mensagemErroEsperada, e.getMessage());
			throw new RegraNegocioException(mensagemErroEsperada);
		}
	}

	private void validarExclusaoComException(String mensagemErro) throws RegraNegocioException {
		try {
			sut.excluir(carro);

		} catch(RegraNegocioException e) {
			assertEquals(mensagemErro, e.getMessage());
			throw new RegraNegocioException(mensagemErro);
		}
	}
}
