package com.algaworks.curso.jpa2.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.algaworks.curso.jpa2.dao.AcessorioDao;
import com.algaworks.curso.jpa2.modelo.Acessorio;
import com.algaworks.curso.jpa2.modelo.builders.AcessorioBuilder;
import com.algaworks.curso.jpa2.services.exception.RegraNegocioException;

public class AcessorioServicesTest {

	private static final String MENSAGEM_ERRO_DESCRICAO_VAZIA = "A descrição do acessório não pode ser vazia";
	private static final String MENSAGEM_ERRO_ACESSORIO_INEXISTENTE = "O acessorio não existe e não pode ser excluído";

	private AcessorioServices sut;

	private Acessorio acessorio;

	@Mock
	private AcessorioDao daoMock;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		acessorio = new AcessorioBuilder().comCodigo(1L)
										.comDescricao("Ar condicionado")
										.construir();

		sut = new AcessorioServices();
		sut.setAcessorioDao(daoMock);
	}

	@Test
	public void deveChamarOMetodoSalvarDoDaoSeEstiverTudoOk() throws Exception {
		sut.salvar(acessorio);

		verify(daoMock).salvar(acessorio);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmAcessorioSemDescricao() throws Exception {
		acessorio.setDescricao(null);

		sut.salvar(acessorio);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDevePersistirUmAcessorioComDescricaoVazia() throws Exception {
		acessorio.setDescricao("  	 ");

		sut.salvar(acessorio);
	}

	@Test
	public void validarMensagemDaExceptionSeAcessorioComDescricaoVazia() throws Exception {
		acessorio.setDescricao("    ");

		try {
			sut.salvar(acessorio);
		} catch(RegraNegocioException e) {
			assertEquals(MENSAGEM_ERRO_DESCRICAO_VAZIA, e.getMessage());
		}
	}

	@Test
	public void validarMensagemDaExceptionSeAcessorioComDescricaoNula() throws Exception {
		acessorio.setDescricao(null);

		try {
			sut.salvar(acessorio);
		} catch(RegraNegocioException e) {
			assertEquals(MENSAGEM_ERRO_DESCRICAO_VAZIA, e.getMessage());
		}
	}

	@Test
	public void deveBuscarTodosOsAcessoriosDoBancoDeDados() throws Exception {
		when(daoMock.buscarTodos()).thenReturn(criarListaDeAcessorios());

		List<Acessorio> acessorios = sut.buscarTodos();

		assertEquals(5, acessorios.size());
	}

	@Test
	public void deveBuscarAcessorioPorCodigo() throws Exception {
		when(daoMock.buscarPorCodigo(1L) ).thenReturn(acessorio);

		Acessorio acessorioTemp = sut.buscarPorCodigo(acessorio.getCodigo());

		verify(daoMock).buscarPorCodigo(1L);
		assertNotNull(acessorioTemp);
	}

	@Test
	public void deveExcluirUmAcessorioPassadoPorParametro() throws Exception {
		when(daoMock.buscarPorCodigo(1L)).thenReturn(acessorio);

		sut.excluir(acessorio);

		verify(daoMock).excluir(acessorio);
	}

	@Test(expected=RegraNegocioException.class)
	public void deveLancarExceptionQuandoTentarExcluirAcessorioQueNaoExiteNoBanco() throws Exception {
		when(daoMock.buscarPorCodigo(1L)).thenReturn(null);

		sut.excluir(acessorio);
	}

	@Test
	public void deveValidarMensagemDaExceptionQuandoTentarExcluirAcessorioInexistenteNoBanco() throws Exception {
		when(daoMock.buscarPorCodigo(1L)).thenReturn(null);

		try {
			sut.excluir(acessorio);
		} catch(RegraNegocioException e) {
			assertEquals(MENSAGEM_ERRO_ACESSORIO_INEXISTENTE, e.getMessage());
		}
	}

	private List<Acessorio> criarListaDeAcessorios() {
		List<Acessorio> acessorios = new ArrayList<Acessorio>();
		acessorios.add(new AcessorioBuilder().comCodigo(1L).comDescricao("Banco de couro").construir());
		acessorios.add(new AcessorioBuilder().comCodigo(2L).comDescricao("Ar Condicionado").construir());
		acessorios.add(new AcessorioBuilder().comCodigo(3L).comDescricao("Direção Hidraulica").construir());
		acessorios.add(new AcessorioBuilder().comCodigo(4L).comDescricao("Vidro Elétrico").construir());
		acessorios.add(new AcessorioBuilder().comCodigo(5L).comDescricao("Alarme").construir());

		return acessorios;
	}
}
