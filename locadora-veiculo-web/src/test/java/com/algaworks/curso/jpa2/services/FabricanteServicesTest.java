package com.algaworks.curso.jpa2.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.algaworks.curso.jpa2.dao.FabricantesDao;
import com.algaworks.curso.jpa2.modelo.Fabricante;
import com.algaworks.curso.jpa2.services.exception.RegraNegocioException;

public class FabricanteServicesTest {

	private static String MENSAGEM_EXCEPTION_SALVAR = "O nome do fabricante é obrigatório";
	private static String MENSAGEM_EXCEPTION_EXCLUIR_INEXISTENTE = "O fabricante não existe e não pode ser excluído";

	private FabricanteServices sut;

	private Fabricante fabricante;

	@Mock
	private FabricantesDao daoMock;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		sut = new FabricanteServices();
		sut.setFabricanteDao(daoMock);

		fabricante = new Fabricante();
		fabricante.setCodigo(1L);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDeveAceitarNomeDeFabricanteNulo() throws Exception {
		fabricante.setNome(null);
		sut.salvar(fabricante);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDeveAceitarNomeDeFabricanteVazio() throws Exception {
		fabricante.setNome("   		      ");
		sut.salvar(fabricante);
	}

	@Test
	public void deveValidarAMensagemLancadaNaExceptionDeSalvarFabricante() {
		fabricante.setNome(null);

		try {
			sut.salvar(fabricante);
		} catch (RegraNegocioException e) {
			assertEquals("Validando se mensagem da Exception condiz com o especificado", MENSAGEM_EXCEPTION_SALVAR, e.getMessage());
		}
	}

	@Test
	public void deveGarantirAChamadaDoMetodoSalvarDoDao() throws Exception {
		fabricante.setNome("Chevrolet");
		sut.salvar(fabricante);

		verify(daoMock).salvar(fabricante);
	}

	@Test
	public void deveBuscarListaDeFabricantesDoBanco() throws Exception {
		when(daoMock.buscarTodos()).thenReturn(criarListaFabricante());

		List<Fabricante> resultado = sut.buscarTodos();

		verify(daoMock).buscarTodos();
		assertEquals(3, resultado.size(), 0.00001);

	}


	@Test
	public void deveBuscarFabricantePorCodigo() throws Exception {
		sut.buscarPorCodigo(1L);

		verify(daoMock).buscarPorCodigo(1L);
	}

	@Test
	public void deveExcluirFabricantePassadoPorParametro() throws Exception {
		when(daoMock.buscarPorCodigo(1L)).thenReturn(fabricante);

		sut.excluir(fabricante);

		verify(daoMock).excluir(fabricante);
	}

	@Test(expected=RegraNegocioException.class)
	public void deveLancarExceptionQuandoTentarExcluirUmFabricanteQueNaoExisteNoBanco() throws Exception {
		when(daoMock.buscarPorCodigo(1L)).thenReturn(null);

		sut.excluir(fabricante);
	}

	@Test
	public void validarMensagemDaExceptionQuandoTentarExcluirUmFabricanteQueNaoExisteNoBanco() throws Exception {
		when(daoMock.buscarPorCodigo(1L)).thenReturn(null);
		try {
			sut.excluir(fabricante);

			fail("Método não lançou a exception que era esperada");
		} catch(RegraNegocioException e) {
			assertEquals(MENSAGEM_EXCEPTION_EXCLUIR_INEXISTENTE, e.getMessage());
		}
	}

	private List<Fabricante> criarListaFabricante() {
		List<Fabricante> fabricantes = new ArrayList<Fabricante>();

		// Adiciona três fabricantes;
		fabricantes.add(new Fabricante());
		fabricantes.add(new Fabricante());
		fabricantes.add(new Fabricante());

		return fabricantes;
	}
}
