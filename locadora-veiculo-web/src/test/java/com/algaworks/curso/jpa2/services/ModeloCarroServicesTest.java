package com.algaworks.curso.jpa2.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.algaworks.curso.jpa2.dao.ModeloCarroDao;
import com.algaworks.curso.jpa2.modelo.Fabricante;
import com.algaworks.curso.jpa2.modelo.ModeloCarro;
import com.algaworks.curso.jpa2.modelo.builders.ModeloCarroBuilder;
import com.algaworks.curso.jpa2.services.exception.RegraNegocioException;

public class ModeloCarroServicesTest {

	private static final String MENSAGEM_ERRO_SEM_FABRICANTE = "O fabricante e obrigatório";
	private static final String MENSAGEM_ERRO_SEM_DESCRICAO = "O nome do modelo é obrigatório";
	private static final String MENSAGEM_ERRO_MODELO_INEXISTENTE = "O modelo de carro não existe e não pode ser excluído";

	private ModeloCarroService sut;

	@Mock
	private ModeloCarroDao daoMock;

	private ModeloCarro modeloCarro;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		modeloCarro = new ModeloCarroBuilder()
									.comCodigo(1L)
									.comDescricao("Fusca")
									.comFabricante("Volkswagen")
									.construir();

		sut = new ModeloCarroService();
		sut.setModeloCarroDao(daoMock);
	}

	@Test
	public void deveSalvarUmModeloDeCarro() throws Exception {
		sut.salvar(modeloCarro);

		verify(daoMock).salvar(modeloCarro);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDeveAceitarUmModeloDeCarroSemFabricante() throws Exception {
		modeloCarro.setFabricante(null);

		sut.salvar(modeloCarro);
	}

	@Test
	public void validarMensagemDaExceptionCasoOModeloNaoSejaSalvoPorFaltaDeFabricante() throws Exception {
		modeloCarro.setFabricante(null);

		try {
			sut.salvar(modeloCarro);
		} catch (RegraNegocioException e) {
			assertEquals(MENSAGEM_ERRO_SEM_FABRICANTE, e.getMessage());
		}
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDeveAceitarUmModeloDeCarroSeDescricaoForNula() throws Exception {
		modeloCarro.setDescricao(null);

		sut.salvar(modeloCarro);
	}

	@Test(expected=RegraNegocioException.class)
	public void naoDeveAceitarUmModeloDeCarrosSeDescricaoForVazia() throws Exception {
		modeloCarro.setDescricao("");
		sut.salvar(modeloCarro);
	}

	@Test
	public void validarMensagemDaExceptionCasoADescricaoDoModeloEstejaNulaOuVazia() throws Exception {
		modeloCarro.setDescricao("  	  ");

		try {
			sut.salvar(modeloCarro);
		} catch (RegraNegocioException e) {
			assertEquals(MENSAGEM_ERRO_SEM_DESCRICAO, e.getMessage());
		}
	}

	@Test
	public void deveBuscarListaDeModelosDoBanco() throws Exception {
		when(daoMock.buscarTodos()).thenReturn(criarListaDeModelos());

		List<ModeloCarro> lista = sut.buscarTodos();

		assertEquals(6, lista.size());
	}

	@Test
	public void deveBuscarTodosOsModelosDeDeterminadoFabricante() throws Exception {
		when(daoMock.buscarTodos()).thenReturn(criarListaDeModelos());

		Fabricante fabricante = new Fabricante();
		fabricante.setNome("Chevrolet");

		List<ModeloCarro> lista = sut.buscarTodos(fabricante);

		assertEquals(3, lista.size());
	}

	@Test
	public void deveBuscarModeloDeCarroPorCodigo() throws Exception {
		sut.buscarPorCodigo(1L);

		verify(daoMock).buscarPorCodigo(1L);
	}

	@Test
	public void deveExcluirModeloDeCarroPassadoPorParametro() throws Exception {
		when(daoMock.buscarPorCodigo(1L)).thenReturn(modeloCarro);

		sut.excluir(modeloCarro);

		verify(daoMock).excluir(modeloCarro);
	}

	@Test(expected=RegraNegocioException.class)
	public void deveLancarExceptionQuandoTentarExcluirUmModeloDeCarroQueNaoExisteNoBanco() throws Exception {
		when(daoMock.buscarPorCodigo(1L)).thenReturn(null);

		sut.excluir(modeloCarro);
	}

	@Test
	public void validarMensagemDaExceptionQuandoTentarExcluirUmModeloDeCarroQueNaoExiste() throws Exception {
		when(daoMock.buscarPorCodigo(1L)).thenReturn(null);

		try {
			sut.excluir(modeloCarro);
		} catch(RegraNegocioException e) {
			assertEquals(MENSAGEM_ERRO_MODELO_INEXISTENTE, e.getMessage());
		}
	}

	private List<ModeloCarro> criarListaDeModelos() {
		List<ModeloCarro> modelos = new ArrayList<ModeloCarro>();
		modelos.add(new ModeloCarroBuilder().comCodigo(1L).comDescricao("Fusca").comFabricante("Volkswagen").construir());
		modelos.add(new ModeloCarroBuilder().comCodigo(2L).comDescricao("HB 20").comFabricante("Hyundai").construir());
		modelos.add(new ModeloCarroBuilder().comCodigo(3L).comDescricao("Vectra").comFabricante("Chevrolet").construir());
		modelos.add(new ModeloCarroBuilder().comCodigo(4L).comDescricao("Astra").comFabricante("Chevrolet").construir());
		modelos.add(new ModeloCarroBuilder().comCodigo(5L).comDescricao("S10").comFabricante("Chevrolet").construir());
		modelos.add(new ModeloCarroBuilder().comCodigo(6L).comDescricao("Classe B").comFabricante("Mercedes Bens").construir());

		return modelos;
	}
}
