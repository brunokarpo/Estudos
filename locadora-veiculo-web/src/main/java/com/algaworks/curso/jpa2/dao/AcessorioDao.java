package com.algaworks.curso.jpa2.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.algaworks.curso.jpa2.modelo.Acessorio;

public class AcessorioDao implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager em;

	public void salvar(Acessorio acessorio) {
		em.merge(acessorio);
	}

	@SuppressWarnings("unchecked")
	public List<Acessorio> buscarTodos() {
		return em.createQuery("from Acessorio").getResultList();
	}

	public Acessorio buscarPorCodigo(Long codigo) {
		return em.find(Acessorio.class, codigo);
	}

	public void excluir(Acessorio acessorio) {
		em.remove(acessorio);
		em.flush();
	}

}
