package com.algaworks.curso.jpa2.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import com.algaworks.curso.jpa2.modelo.Carro;

public class CarroDao {

	@Inject
	private EntityManager em;

	public void salvar(Carro carro) {
		em.merge(carro);
	}

	@SuppressWarnings("unchecked")
	public List<Carro> buscarTodos() {
		return em.createQuery("from Carro").getResultList();
	}

	public Carro buscarPorCodigo(Long codigo) {
		return em.find(Carro.class, codigo);
	}

	public void excluir(Carro carro) {
		em.remove(carro);
		em.flush();
	}

	public Carro buscarCarroComAcessorios(Long codigo) {
		try {
			return (Carro) em.createQuery("select c from Carro c JOIN c.acessorios a where c.codigo = ?")
					.setParameter(1, codigo)
					.getSingleResult();
		} catch(NoResultException e) {
			return buscarPorCodigo(codigo);
		}
	}


}
