package com.algaworks.curso.jpa2.services;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.algaworks.curso.jpa2.dao.CarroDao;
import com.algaworks.curso.jpa2.modelo.Acessorio;
import com.algaworks.curso.jpa2.modelo.Carro;
import com.algaworks.curso.jpa2.services.exception.RegraNegocioException;
import com.algaworks.curso.jpa2.util.jpa.Transactional;

public class CarroServices implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CarroDao carroDao;

	@Inject
	private ModeloCarroService modeloServices;

	@Inject
	private AcessorioServices acessorioServices;

	@Transactional
	public void salvar(Carro carro) throws RegraNegocioException {

		validarInformacoesCarro(carro);

		carroDao.salvar(carro);
	}

	public List<Carro> buscarTodos() {
		return carroDao.buscarTodos();
	}

	public Carro buscarPorCodigo(Long codigo) {
		return carroDao.buscarPorCodigo(codigo);
	}

	@Transactional
	public void excluir(Carro carro) throws RegraNegocioException {
		Carro temp = buscarPorCodigo(carro.getCodigo());

		if(temp == null) {
			throw new RegraNegocioException("O carro não existe no Banco de Dados e não pode ser excluído");
		}

		carroDao.excluir(temp);
	}

	public Carro buscarCarroComAcessorios(Long codigo) {
		return carroDao.buscarCarroComAcessorios(codigo);
	}

	private void validarInformacoesCarro(Carro carro) throws RegraNegocioException {
		if(carro.getPlaca() == null || "".equals(carro.getPlaca().trim())) {
			throw new RegraNegocioException("O carro precisa ter uma placa");
		}

		if(carro.getChassi() == null || "".equals(carro.getChassi().trim())) {
			throw new RegraNegocioException("O carro precisa ter um chassi");
		}

		if(carro.getCor() == null || "".equals(carro.getCor().trim())) {
			throw new RegraNegocioException("Informe a cor do carro");
		}

		if(carro.getValorDiaria() == null || carro.getValorDiaria().doubleValue() <= 0.0) {
			throw new RegraNegocioException("Valor da diária inválido. Informe um valor positivo diferente de zero");
		}
		if(carro.getModeloCarro() == null) {
			throw new RegraNegocioException("O carro precisa ter um modelo");
		}
		if(!modeloServices.buscarTodos().contains(carro.getModeloCarro())) {
			throw new RegraNegocioException("Modelo informado não existe");
		}
		if(!acessorioServices.buscarTodos().containsAll(carro.getAcessorios())) {
			throw new RegraNegocioException("O(s) acessório(s) " + listaAcessoriosInvalidos(carro.getAcessorios()) + " não existe(m)");
		}
	}

	private String listaAcessoriosInvalidos(List<Acessorio> acessorios) {
		StringBuilder sb = new StringBuilder();
		int i = 1;

		for(Acessorio acessorio : acessorios) {
			if(!acessorioServices.buscarTodos().contains(acessorio)) {
				sb.append(acessorio.getDescricao());
				if(i != acessorios.size()) {
					sb.append(", ");
				}
			}
			i++;
		}
		return sb.toString();
	}


	/**
	 * Método para inversão de controle e auxílio nos testes;
	 * @param carroDao
	 */
	void setCarroDao(CarroDao carroDao) {
		this.carroDao = carroDao;
	}

	/**
	 * Método para inversão de controle e auxílio nos testes;
	 * @param modeloServices
	 */
	void setModeloCarroServices(ModeloCarroService modeloServices) {
		this.modeloServices = modeloServices;
	}

	/**
	 * Método para inversão de controle e auxílio nos testes;
	 * @param acessorioServices
	 */
	void setAcessorioServices(AcessorioServices acessorioServices) {
		this.acessorioServices = acessorioServices;
	}

}
