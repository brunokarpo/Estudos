package com.algaworks.curso.jpa2.modelo.builders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.algaworks.curso.jpa2.modelo.Acessorio;
import com.algaworks.curso.jpa2.modelo.Carro;
import com.algaworks.curso.jpa2.modelo.ModeloCarro;

public class CarroBuilder {

	private Carro instancia;
	private List<Acessorio> listaAcessorios;

	public CarroBuilder() {
		listaAcessorios = new ArrayList<Acessorio>();
		instancia = new Carro();
	}

	public CarroBuilder comChassi(String chassi) {
		instancia.setChassi(chassi);
		return this;
	}

	public CarroBuilder comPlaca(String placa) {
		instancia.setPlaca(placa);
		return this;
	}

	public CarroBuilder comCor(String cor) {
		instancia.setCor(cor);
		return this;
	}

	public CarroBuilder comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		return this;
	}

	public CarroBuilder comValorDiaria(BigDecimal valorDiaria) {
		instancia.setValorDiaria(valorDiaria);
		return this;
	}

	public CarroBuilder comModelo(ModeloCarro modeloCarro) {
		instancia.setModeloCarro(modeloCarro);
		return this;
	}

	public CarroBuilder comAcessorio(String nomeAcessorio) {
		listaAcessorios.add(new AcessorioBuilder().comDescricao(nomeAcessorio).construir());
		return this;
	}

	public CarroBuilder comAcessorios(List<Acessorio> acessorios) {
		this.listaAcessorios = acessorios;
		return this;
	}

	public Carro construir() {
		instancia.setAcessorios(listaAcessorios);
		return instancia;
	}
}
