package com.algaworks.curso.jpa2.modelo.builders;

import java.math.BigDecimal;

import com.algaworks.curso.jpa2.modelo.Aluguel;
import com.algaworks.curso.jpa2.modelo.ApoliceSeguro;
import com.algaworks.curso.jpa2.modelo.Carro;

public class AluguelBuilder {

	private Aluguel instancia;

	public AluguelBuilder() {
		instancia = new Aluguel();
	}

	public AluguelBuilder comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		return this;
	}

	public AluguelBuilder comValorTotal(BigDecimal valorTotal) {
		instancia.setValorTotal(valorTotal);
		return this;
	}

	public AluguelBuilder comCarro(Carro carro) {
		instancia.setCarro(carro);
		return this;
	}

	public AluguelBuilder comApoliceSeguro(ApoliceSeguro apoliceSeguro) {
		instancia.setApoliceSeguro(apoliceSeguro);
		return this;
	}

	public Aluguel construir() {
		return instancia;
	}
}
