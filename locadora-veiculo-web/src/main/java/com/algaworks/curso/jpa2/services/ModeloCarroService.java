package com.algaworks.curso.jpa2.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.algaworks.curso.jpa2.dao.ModeloCarroDao;
import com.algaworks.curso.jpa2.modelo.Fabricante;
import com.algaworks.curso.jpa2.modelo.ModeloCarro;
import com.algaworks.curso.jpa2.services.exception.RegraNegocioException;
import com.algaworks.curso.jpa2.util.jpa.Transactional;

public class ModeloCarroService implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private ModeloCarroDao modeloCarroDao;

	@Transactional
	public void salvar(ModeloCarro modeloCarro) throws RegraNegocioException {
		if(modeloCarro.getFabricante() == null) {
			throw new RegraNegocioException("O fabricante e obrigatório");
		}

		if(modeloCarro.getDescricao() == null || "".equals(modeloCarro.getDescricao().trim())) {
			throw new RegraNegocioException("O nome do modelo é obrigatório");
		}

		modeloCarroDao.salvar(modeloCarro);
	}


	public List<ModeloCarro> buscarTodos() {
		return buscarTodos(null);
	}

	public List<ModeloCarro> buscarTodos(Fabricante fabricante) {
		List<ModeloCarro> todos = modeloCarroDao.buscarTodos();
		List<ModeloCarro> aRetornar = new ArrayList<ModeloCarro>();

		if(fabricante == null) {
			return todos;

		} else {
			for(ModeloCarro modelo : todos) {
				if(modelo.getFabricante().getNome().equals(fabricante.getNome())) {
					aRetornar.add(modelo);
				}
			}
		}
		return aRetornar;
	}

	public ModeloCarro buscarPorCodigo(Long codigo) {
		return modeloCarroDao.buscarPorCodigo(codigo);
	}

	@Transactional
	public void excluir(ModeloCarro modeloCarro) throws RegraNegocioException {
		ModeloCarro modelo = buscarPorCodigo(modeloCarro.getCodigo());
		if(modelo == null) {
			throw new RegraNegocioException("O modelo de carro não existe e não pode ser excluído");
		}
		modeloCarroDao.excluir(modelo);
	}

	/**
	 * Método auxiliar para inversão de controle para execução de testes
	 * @param modeloCarroDao
	 */
	void setModeloCarroDao(ModeloCarroDao modeloCarroDao) {
		this.modeloCarroDao = modeloCarroDao;
	}

}
