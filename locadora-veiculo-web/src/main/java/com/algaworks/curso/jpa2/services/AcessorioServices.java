package com.algaworks.curso.jpa2.services;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.algaworks.curso.jpa2.dao.AcessorioDao;
import com.algaworks.curso.jpa2.modelo.Acessorio;
import com.algaworks.curso.jpa2.services.exception.RegraNegocioException;
import com.algaworks.curso.jpa2.util.jpa.Transactional;

public class AcessorioServices implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private AcessorioDao acessorioDao;

	@Transactional
	public void salvar(Acessorio acessorio) throws RegraNegocioException {
		if(acessorio.getDescricao() == null ||
				"".equals(acessorio.getDescricao().trim())) {

			throw new RegraNegocioException("A descrição do acessório não pode ser vazia");
		}
		acessorioDao.salvar(acessorio);
	}

	/**
	 * Método para inversão de controle para o DAO.
	 *
	 * Facilitar os testes unitários;
	 * @param acessorioDao
	 */
	void setAcessorioDao(AcessorioDao acessorioDao) {
		this.acessorioDao = acessorioDao;
	}

	public List<Acessorio> buscarTodos() {
		return acessorioDao.buscarTodos();
	}

	public Acessorio buscarPorCodigo(Long codigo) {
		return acessorioDao.buscarPorCodigo(codigo);
	}

	@Transactional
	public void excluir(Acessorio acessorio) throws RegraNegocioException {
		Acessorio temp = buscarPorCodigo(acessorio.getCodigo());
		if(temp == null) {
			throw new RegraNegocioException("O acessorio não existe e não pode ser excluído");
		}
 		acessorioDao.excluir(temp);
	}
}
