package com.algaworks.curso.jpa2.services;

import java.io.Serializable;

import javax.inject.Inject;

import com.algaworks.curso.jpa2.dao.AluguelDao;
import com.algaworks.curso.jpa2.modelo.Aluguel;
import com.algaworks.curso.jpa2.services.exception.RegraNegocioException;
import com.algaworks.curso.jpa2.util.jpa.Transactional;

public class AluguelService implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Double ZERO = 0.0;

	@Inject
	private AluguelDao aluguelDao;

	@Inject
	private CarroServices carroServices;

	@Transactional
	public void salvar(Aluguel aluguel) throws RegraNegocioException {
		validarAluguel(aluguel);

		aluguelDao.salvar(aluguel);
	}

	private void validarAluguel(Aluguel aluguel) throws RegraNegocioException {
		if(aluguel.getValorTotal() == null || aluguel.getValorTotal().doubleValue() <= 0.0) {
			throw new RegraNegocioException("Valor total do Aluguel inválido. Informe um valor positivo diferente de zero");
		}
		if(aluguel.getCarro() == null) {
			throw new RegraNegocioException("Um aluguel não pode ser persistido se não tiver um carro. Informe um carro");
		}
		if( carroServices.buscarPorCodigo(aluguel.getCarro().getCodigo()) == null) {
			throw new RegraNegocioException("Carro não existe no banco. Informe um carro válido;");
		}
		if( apoliceComSeguro(aluguel) ) {
			if(aluguel.getApoliceSeguro().getValorFranquia().doubleValue() <= ZERO) {
				throw new RegraNegocioException("Apolice contém seguros e não pode estar com a franquia zerada");
			}
		} else {
			if(aluguel.getApoliceSeguro().getValorFranquia().doubleValue() > ZERO) {
				throw new RegraNegocioException("Apolice sem seguros. Não deve ser cobrado franquia");
			}
		}
	}

	private boolean apoliceComSeguro(Aluguel aluguel) {
		return aluguel.getApoliceSeguro().getProtecaoCausasNaturais()
				|| aluguel.getApoliceSeguro().getProtecaoRoubo()
				|| aluguel.getApoliceSeguro().getProtecaoTerceiros();
	}

	/**
	 * Método para inverter controle e inserir um aluguelDao customizado;
	 *
	 * Auxiliar testes unitários
	 * @param aluguelDao
	 */
	void setAluguelDao(AluguelDao aluguelDao) {
		this.aluguelDao = aluguelDao;
	}

	void setCarroService(CarroServices carroServices) {
		this.carroServices = carroServices;
	}

}
