package com.algaworks.curso.jpa2.services;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.algaworks.curso.jpa2.dao.FabricantesDao;
import com.algaworks.curso.jpa2.modelo.Fabricante;
import com.algaworks.curso.jpa2.services.exception.RegraNegocioException;
import com.algaworks.curso.jpa2.util.jpa.Transactional;

public class FabricanteServices implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private FabricantesDao fabricantesDao;

	@Transactional
	public void salvar(Fabricante fabricante) throws RegraNegocioException {

		if(fabricante.getNome() == null || "".equals( fabricante.getNome().trim() ) ) {
			throw new RegraNegocioException("O nome do fabricante é obrigatório");
		}

		fabricantesDao.salvar(fabricante);
	}


	public List<Fabricante> buscarTodos() {
		return fabricantesDao.buscarTodos();
	}

	@Transactional
	public void excluir(Fabricante fabricante) throws RegraNegocioException{
		Fabricante fabricanteTemp = buscarPorCodigo(fabricante.getCodigo());

		if(fabricanteTemp == null) {
			throw new RegraNegocioException("O fabricante não existe e não pode ser excluído");
		}

		fabricantesDao.excluir(fabricanteTemp);
	}

	public Fabricante buscarPorCodigo(Long codigo) {
		return fabricantesDao.buscarPorCodigo(codigo);
	}

	/**
	 * Injeçao de dependencia para auxiliar testes;
	 * @param fabricantesDao
	 */
	public void setFabricanteDao(FabricantesDao fabricantesDao) {
		this.fabricantesDao = fabricantesDao;
	}

}
