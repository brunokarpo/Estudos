package com.algaworks.curso.jpa2.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.algaworks.curso.jpa2.modelo.ModeloCarro;

public class ModeloCarroDao implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager em;

	public void salvar(ModeloCarro modeloCarro) {
		em.merge(modeloCarro);
	}

	@SuppressWarnings("unchecked")
	public List<ModeloCarro> buscarTodos() {
		return em.createQuery("from ModeloCarro").getResultList();
	}

	public ModeloCarro buscarPorCodigo(Long codigo) {
		return em.find(ModeloCarro.class, codigo);
	}

	public void excluir(ModeloCarro modeloCarro) {
		em.remove(modeloCarro);
		em.flush();
	}

}
