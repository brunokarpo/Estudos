package com.algaworks.curso.jpa2.modelo.builders;

import com.algaworks.curso.jpa2.modelo.Acessorio;

public class AcessorioBuilder {

	private Acessorio instancia;

	public AcessorioBuilder() {
		instancia = new Acessorio();
	}

	public AcessorioBuilder comDescricao(String descricao) {
		instancia.setDescricao(descricao);
		return this;
	}

	public AcessorioBuilder comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		return this;
	}

	public Acessorio construir() {
		return instancia;
	}

}
