package com.algaworks.curso.jpa2.modelo.builders;

import java.math.BigDecimal;

import com.algaworks.curso.jpa2.modelo.ApoliceSeguro;

public class ApoliceSeguroBuilder {

	private ApoliceSeguro instancia;

	public ApoliceSeguroBuilder() {
		instancia = new ApoliceSeguro();
	}

	public ApoliceSeguroBuilder comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		return this;
	}

	public ApoliceSeguroBuilder comProtecaoCausasNaturais(Boolean protecaoCausasNaturais) {
		instancia.setProtecaoCausasNaturais(protecaoCausasNaturais);
		return this;
	}

	public ApoliceSeguroBuilder comProtecaoRoubo(Boolean protecaoRoubo) {
		instancia.setProtecaoRoubo(protecaoRoubo);
		return this;
	}

	public ApoliceSeguroBuilder comProtecaoTerceiros(Boolean protecaoTerceiros) {
		instancia.setProtecaoTerceiros(protecaoTerceiros);
		return this;
	}

	public ApoliceSeguroBuilder comValorFranquia(BigDecimal valorFranquia) {
		instancia.setValorFranquia(valorFranquia);
		return this;
	}

	public ApoliceSeguro construir() {
		return instancia;
	}

}
