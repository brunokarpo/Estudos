package com.algaworks.curso.jpa2.modelo.builders;

import com.algaworks.curso.jpa2.modelo.Fabricante;
import com.algaworks.curso.jpa2.modelo.ModeloCarro;

public class ModeloCarroBuilder {

	private ModeloCarro instancia;

	public ModeloCarroBuilder() {
		instancia = new ModeloCarro();
	}

	public ModeloCarroBuilder comDescricao(String descricao) {
		instancia.setDescricao(descricao);
		return this;
	}

	public ModeloCarroBuilder comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		return this;
	}

	public ModeloCarroBuilder comFabricante(String nomeFabricante) {
		Fabricante fabricante = new Fabricante();
		fabricante.setNome(nomeFabricante);
		instancia.setFabricante(fabricante);
		return this;
	}

	public ModeloCarro construir() {
		return instancia;
	}

}
