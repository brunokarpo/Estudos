package com.algaworks.curso.jpa2.controller.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.algaworks.curso.jpa2.dao.FabricantesDao;
import com.algaworks.curso.jpa2.modelo.Fabricante;
import com.algaworks.curso.jpa2.util.cdi.CDIServiceLocator;

@FacesConverter(forClass=Fabricante.class)
public class FabricanteConverter implements Converter {

	private FabricantesDao fabricanteDao;

	public FabricanteConverter() {
		this.fabricanteDao = CDIServiceLocator.getBean(FabricantesDao.class);
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Fabricante retorno = null;

		if(value != null) {
			retorno = fabricanteDao.buscarPorCodigo(Long.valueOf(value));
		}

		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {

		if(value != null) {
			Long codigo = ((Fabricante) value).getCodigo();
			String retorno = (codigo == null ? "" : codigo.toString());
			return retorno;
		}

		return "";
	}

}
