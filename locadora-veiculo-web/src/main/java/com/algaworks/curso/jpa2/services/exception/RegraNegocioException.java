package com.algaworks.curso.jpa2.services.exception;

public class RegraNegocioException extends Exception {

	private static final long serialVersionUID = 1L;

	public RegraNegocioException(String message) {
		super(message);
	}

}
